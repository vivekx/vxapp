export function fetchBreweriesSuccess(breweries) {
    return {
        type: 'FETCH_BREWERIES_SUCCESS',
        breweries: breweries
    }
}

export function fetchBreweriesError(error) {
    return {
        type: 'FETCH_BREWERIES_ERROR',
        error: error
    }
}

export function fetchBrewerySuccess(brewery) {
    return {
        type: 'FETCH_BREWERY_SUCCESS',
        brewery: brewery
    }
}

export function fetchBreweryError(error) {
    return {
        type: 'FETCH_BREWERY_ERROR',
        error: error
    }
}

export function fetchBreweries() {
    
    return dispatch => {
            fetch('https://api.openbrewerydb.org/breweries')
            .then((response)=>response.json())
            .then((response) => {
                dispatch(fetchBreweriesSuccess(response))
                console.log(response)
                return response
            })
            .catch(error => dispatch(fetchBreweriesError(error)))
            

    }
}

export function fetchBrewery(id) {
    
    return dispatch => {
            fetch('https://api.openbrewerydb.org/breweries/'+id)
            .then((response)=>response.json())
            .then((response) => {
                dispatch(fetchBrewerySuccess(response))
                console.log(response)
                return response
            })
            .catch(error => dispatch(fetchBreweryError(error)))
            

    }
}

