const initialState = {
    breweries: [],
    brewery: null,
    error: undefined
}

export default function breweriesReducer(state = initialState, action) {
    console.log(action)
    switch(action.type) {
        case 'FETCH_BREWERIES_SUCCESS':
            return {
                ...state,
                pending: false,
                breweries: action.breweries
            }
        case 'FETCH_BREWERIES_ERROR':
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case 'FETCH_BREWERY_SUCCESS':
            return {
                ...state,
                pending: false,
                brewery: action.brewery
            }
        case 'FETCH_BREWERY_ERROR':
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default: 
            return state;
    }
}
