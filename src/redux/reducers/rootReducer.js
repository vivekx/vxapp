import {combineReducers} from 'redux'
import auth from './authReducer'
import breweriesReducer from './breweriesReducer'

const rootReducer = combineReducers({
    auth,
    breweriesReducer
})

export default rootReducer