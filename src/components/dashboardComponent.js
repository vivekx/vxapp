import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, KeyboardAvoidingView, FlatList, ImageBackground } from 'react-native'
import {connect} from 'react-redux'
import * as actions from '../redux/actions/breweriesAction'
import NavigationService from '../services/navigationService'

class Dashboard extends React.Component {

    constructor (props) 
    {
        super(props)
    }

    static navigationOptions = {
        title: 'BREWERIES',
        headerLeft: null,
        headerTitleStyle: {
            flex:1,
            color: '#00005A',
            fontWeight: '600',
            textAlign:"center",
        },
    }

    render()
    {
        return (
            <ImageBackground source={require('../images/beer.jpg')} style={styles.main}>
                <FlatList
                    style={{ marginTop: 40}}
                    data={this.props.breweries}
                    renderItem={
                        ({item}) => <TouchableOpacity onPress={() => NavigationService.navigate('view', item)} style={{flex:1,marginBottom:10,justifyContent:'center',alignItems:'center'}}>
                                        <View style={styles.list}>        
                                            <Text style={styles.nameText}>{item.name}</Text>
                                            <View style={styles.subContent}>
                                                <Text style={styles.otherText}>{item.city}</Text>
                                                <Text style={styles.otherText}>{item.phone}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>}

                />
            </ImageBackground>
        );
    }

    componentDidMount()
    {
        this.props.fetchBreweries()
    }
  
}

const mapStateToProps = state => ({
    error: state.breweriesReducer.error,
    breweries: state.breweriesReducer.breweries,
})

const mapDispatchToProps = dispatch => ({
    fetchBreweries:() => dispatch(actions.fetchBreweries())
})

export default connect(mapStateToProps,mapDispatchToProps)(Dashboard)


const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#63b5fb'
    },
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    list: {
        flex:1, 
        width: '90%', 
        backgroundColor:'rgba(38, 38, 23, 0.8)',
        borderRadius: 5,
        borderWidth:1,
        borderColor: '#b5e2fd', 
        padding: 10
    },
    nameText: {
        flex: 1,
        fontWeight: 'bold',
        fontSize: 16,
        textTransform: 'uppercase',
        // color:'#535353'
        color: 'white'
    },
    otherText: {
        fontSize: 14,
        color:'white'
    },
    subContent: {
        flex: 3,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor:'rgba(38, 38, 23, 0.8)',
        borderRadius: 5,
        borderWidth:1,
        borderColor: '#b5e2fd',
        padding: 5
    }
  
});
