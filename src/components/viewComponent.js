import React from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ImageBackground, Linking } from 'react-native'
import {connect} from 'react-redux'
import * as actions from '../redux/actions/breweriesAction'

class ViewBrew extends React.Component {

    constructor (props) 
    {
        super(props)
    }

    render()
    {
        return (
            <ImageBackground source={require('../images/beer.jpg')} style={styles.main}>
                {this.props.brewery && 
                    
                    <View style={styles.item}>
                        <Text style={styles.head}>{this.props.brewery.name}</Text>
                        <View style={styles.sub}>
                            <TouchableOpacity style={styles.detail}>
                                <Text style={styles.nomalText}>Country:</Text>
                                <Text style={styles.nomalText}>{this.props.brewery.country}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.detail}>
                                <Text style={styles.nomalText}>Type:</Text>
                                <Text style={styles.nomalText}>{this.props.brewery.brewery_type}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.sub}>
                            <TouchableOpacity style={styles.detail}>
                                <Text style={styles.nomalText}>{this.props.brewery.street}</Text>
                                <Text style={styles.nomalText}>{this.props.brewery.city}</Text>
                                <Text style={styles.nomalText}>{this.props.brewery.state}</Text>
                                <Text style={styles.nomalText}>{this.props.brewery.postal_code}</Text> 
                            </TouchableOpacity>
                        </View>
                        <View style={styles.sub}>
                            <TouchableOpacity style={styles.detail}>
                                <Text style={styles.nomalText}>PHONE: {this.props.brewery.phone}</Text>
                                        
                            </TouchableOpacity>
                        </View>
                         <View style={styles.sub}>
                            <TouchableOpacity style={styles.detail} onPress={ ()=> Linking.openURL(this.props.brewery.website_url) }>
                                <Text style={styles.nomalText}>WEBSITE</Text>
                                        
                            </TouchableOpacity>
                        </View>
                    </View>    
                }
               
            </ImageBackground>
        );
    }

    componentDidMount()
    {
        this.props.fetchBrewery(this.props.navigation.state.params.id)
    }

  
}

const mapStateToProps = state => ({
    error: state.breweriesReducer.error,
    brewery: state.breweriesReducer.brewery,
})

const mapDispatchToProps = dispatch => ({
    fetchBrewery:(id) => dispatch(actions.fetchBrewery(id))
})

export default connect(mapStateToProps,mapDispatchToProps)(ViewBrew)


const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#63b5fb'
    },
    item: {
        flex:1,
        backgroundColor:'rgba(38, 38, 23, 0.8)',
        borderRadius: 5,
        borderWidth:1,
        borderColor: '#b5e2fd',  
        padding: 10,
        margin: 30
    },
    head: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
        textTransform: 'uppercase',
    },
    sub: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    detail: {
        flex:1,
        borderRadius: 5,
        borderWidth:1,
        borderColor: '#b5e2fd',
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nomalText:{
        fontSize: 14,
        color: 'white',
        textAlign:'center'
    }

  
});
